<?php

function recoge($valor) {
    if (isset($_REQUEST[$valor])) { // isset control null
	$resultado = htmlspecialchars(trim(strip_tags($_REQUEST[$valor])), ENT_QUOTES, "UTF-8");
    } else {
	$resultado = "";
    }
    if (get_magic_quotes_gpc()) {
	$resultado = stripslashes($resultado);
    }
    return $resultado;
}

?>
