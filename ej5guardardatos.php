<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
	<title>Ejercicio 5 - Datos películas</title>
	<meta charset="UTF-8">
    </head>
    <body>
	<?php

	function recoge($valor) {
	    if (isset($_REQUEST[$valor])) { // isset control null
		$resultado = htmlspecialchars(trim(strip_tags($_REQUEST[$valor])), ENT_QUOTES, "UTF-8");
	    } else {
		$resultado = "";
	    }
	    if (get_magic_quotes_gpc()) {
		$resultado = stripslashes($resultado);
	    }
	    return $resultado;
	}

	$titulo = recoge("titulo");
	$director = recoge("director");
	$genero = recoge("genero");
	if (($titulo != "") && ($director != "")) {
	    echo("Título: " . $titulo . " Director: " . $director . " Género: " . $genero . "<br><br>");
	    $pelicula = "Título: " . $titulo . " Director: " . $director . " Género: " . $genero;
	    $archivo = fopen("ej5datos.txt", "w");
	    if ($archivo) {
		fwrite($archivo, $pelicula);
		echo("Se han guardado los datos correctamente<br><br>");
	    } else {
		echo("No se han podido guardar los datos<br><br>");
	    }
	    fclose($archivo);
	} else {
	    echo("No se han recibido los datos correctamente<br><br>");
	}
	?>
	<a href="ej5ejercicio5.html">Volver</a><br>
	
    </body>
</html>