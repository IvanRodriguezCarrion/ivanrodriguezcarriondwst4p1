<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 2 - Álbum de fotos</title>
    </head>
    <body>
	<?php
	if (is_uploaded_file($_FILES['foto']['tmp_name'])) {
	    $max = 200000;
	    $extension = $_FILES['foto']['type'];
	    $size = $_FILES['foto']['size'];	
	    $carpeta = "imagenes/";
	    $archivo = $_FILES['foto']['name'];
	    $ruta = $carpeta . $archivo;
	    
	    if (is_file($ruta)) {
		$fecha = time();
		$archivo = $fecha . "-" . $archivo;
	    }
	    
	    if ($size > $max) {
		echo("La foto que estás subiendo supera el máximo permitido (200KB)".PHP_EOL);
	    } elseif ($extension != 'image/jpeg' && $extension != 'image/gif') {
		echo("Sólo se permiten archivos GIF y JPG".PHP_EOL);
	    } else {
		move_uploaded_file($_FILES['foto']['tmp_name'], $carpeta . $archivo);
		echo ("Foto subida con éxito".PHP_EOL);
	    }
	} else {
	    echo ("ERROR: No se ha podido subir la foto.<br/>");
	}
	?>
	<br/><br/>
	<form action='ej2tabla.php' method='get'>
	    <p>Clic para mostrar imágenes del álbum	
		<input type="submit" value="Ver fotos" name="mostrar"/></p>
	</form>
    </body>
</html>
