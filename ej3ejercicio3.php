<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 3 - Libro de visitas</title>
    </head>
    <body>
	<h1>Libro de visitas de Ringo - Deje su comentario</h1>
	<form name="libro" method="post" onsubmit="<?php insertar(); ?>" enctype="multipart/form-data">
	    <table>
		<tr>
		    <td>
			<strong>Tu comentario</strong><br />
			<textarea name="comentarios" cols="50" rows="5" style="height: 200" tabindex="1"></textarea>
		    </td>
		    <td rowspan="4">
			<img  src="imagenes/ringo1.jpg">
		    </td>
		</tr>
		<tr>
		    <td>
			<strong>Tu Nombre</strong><br />
			<input type="text" name="nombre" size="22" tabindex="2" />
		    </td>
		</tr>
		<tr>
		    <td>
			<strong>Tu email</strong><br />
			<input type="text" name="email" size="22" tabindex="3" />
		    </td>
		</tr>
		<tr>
		    <td>
			<input type="submit" name="enviar" value="Enviar comentario" />
		    </td>
		</tr>
	    </table>
	</form>
	<h2>Comentarios de fans de Ringo:</h2>
	<?php

	function comentarios() {
	    if (!file_exists("ej3comentarios.txt")) {
		$archivo = fopen("ej3comentarios.txt", "w") or die("ERROR: No se ha podido crear el archivo.");
		$comenta = "<br/>";
		fwrite($archivo, $comenta);
		fclose($archivo);
	    }
	    $archivo = fopen("ej3comentarios.txt", "r");
	    $size = filesize("ej3comentarios.txt");
	    $comentarios = fread($archivo, $size);
	    fclose($archivo);
	    clearstatcache();
	    echo $comentarios;
	}

	function insertar() {
	    include("ej3comentarios.php");
	    $comentarios = recoge("comentarios");
	    $nombre = recoge("nombre");
	    $email = recoge("email");
	    $fecha = date('y-m-d');

	    if ($comentarios != "" && $nombre != "" && $email != "") { //Control de campos vacíos
		$archivo = fopen("ej3comentarios.txt", "r");
		$tamano = filesize("ej3comentarios.txt");
		$comentario = fread($archivo, $tamano);
		fclose($archivo);
		$archivo = fopen("ej3comentarios.txt", "w");
		$frase = ("<strong>" . $nombre . "</strong>" . "(<a href='mailto:$email'>$email</a>)" . " escrito el " . $fecha . ";" . "<br/>" . $comentarios . "<br/>" . "\r\n");
		$resultado = $frase . $comentario;
		fwrite($archivo, $resultado);
		fclose($archivo);
		clearstatcache();
	    }
	}
	comentarios();
	?>
    </body>
</html>

