<!DOCTYPE html>

<html >
    <head>
	<meta charset="utf-8" />
	<title>Formulario 2</title>
	<link href="estilo.css" rel="stylesheet" type="text/css"/>
    </head>

    <body>
	<?php

	function recoge($valor) {
	    if (isset($_REQUEST[$valor])) {
		$resultado = htmlspecialchars(trim(strip_tags($_REQUEST[$valor])), ENT_QUOTES, "UTF-8");
	    } else {
		$resultado = "";
	    }
	    return $resultado;
	}

	function formulario() {
	    $nombre = recoge("nombre");
	    $nick = recoge("nick");
	    $email = recoge("email");
	    $pass = recoge("pass");
	    $repass = recoge("repass");
	    $cineOn = recoge("cine");
	    if ($cineOn == "on") {
		$cine = "cine";
	    } else {
		$cine = "";
	    }
	    $literaturaOn = recoge("literatura");
	    if ($literaturaOn == "on") {
		$literatura = "literatura";
	    } else {
		$literatura = "";
	    }
	    $musicaOn = recoge("musica");
	    if ($musicaOn == "on") {
		$musica = "musica";
	    } else {
		$musica = "";
	    }
	    
	    //Comprobaciones campos vacíos:
	    if ($nombre == "" || $nick == "" || $email == "" || $pass == "") {
		echo("Alguno de los campos no se ha rellenado.");
	    } else {
		if ($pass == $repass) {
		    if (is_uploaded_file($_FILES['foto']['tmp_name'])) {
			$max = 200000;
			$extension = $_FILES['foto']['type'];
			$size = $_FILES['foto']['size'];
			$carpeta = "imagenes/";
			$archivo = $_FILES['foto']['name'];
			$ruta = $carpeta . $archivo;

			if (is_file($ruta)) {
			    $fecha = time();
			    $archivo = $fecha . "-" . $archivo;
			}

			if ($size > $max) {
			    echo("La foto que estás subiendo supera el máximo permitido (200KB)" . PHP_EOL);
			} elseif ($extension != 'image/jpeg' && $extension != 'image/gif') {
			    echo("Sólo se permiten archivos GIF y JPG" . PHP_EOL);
			} else {
			    move_uploaded_file($_FILES['foto']['tmp_name'], $carpeta . $archivo);
			    $archivo = fopen("F2_ivanrodriguez.txt", "w");
			    $frase = $nombre . ";" . $nick . ";" . $email . ";" . $pass . ";" . $repass . ";" . $cine . ";" . $literatura . ";" . $musica . ";" . $ruta . ";" . "\r\n";
			    fwrite($archivo, $frase);
			    fclose($archivo);
			    echo ("Datos grabados con éxito" . PHP_EOL);
			}
		    } else {
			echo ("ERROR: No se han podido grabar los datos. Falta fichero de imagen.<br/>");
		    }
		} else {
		    echo("Las contraseñas no coinciden");
		}
	    }
	}
	formulario();
	?>
	<form method="post" action="F2_ivanrodriguez.php">
	    <input type='button' value='Volver' onclick='history.back()'/>	
	</form>
    </body>
</html>