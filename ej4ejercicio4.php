<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
	<title>Ejercicio 4 - Datos de alumnos</title>
	<meta charset="UTF-8">
    </head>
    <body>
	<?php

	function recoge($valor) {
	    if (isset($_REQUEST[$valor])) { // isset control null
		$resultado = htmlspecialchars(trim(strip_tags($_REQUEST[$valor])), ENT_QUOTES, "UTF-8");
	    } else {
		$resultado = "";
	    }
	    if (get_magic_quotes_gpc()) {
		$resultado = stripslashes($resultado);
	    }
	    return $resultado;
	}

	$nombre = recoge("nombre");
	$telefono = recoge("telefono");
	$matriculado = recoge("matriculado");
	$ensenanza = recoge("enseñanza");
	$validar = recoge("mostrar");
	if ($validar == "Por Pantalla") {
	    if ($matriculado == "on") {
		print("El alumno " . $nombre . ". Con telefono " . $telefono . ", esta matriculado en un " . $ensenanza);
	    } else {
		print("El alumno " . $nombre . ". Con telefono " . $telefono . ", no esta matriculado.");
	    }
	}
	if ($validar == "En Archivo datos.txt") {
	    if ($matriculado == "on") {
		$frase = "El alumno " . $nombre . ". Con telefono " . $telefono . ", esta matriculado en un " . $ensenanza . "\r\n";
	    } else {
		$frase = "El alumno " . $nombre . ". Con telefono " . $telefono . ", no esta matriculado.";
	    }
	    $archivo = fopen("ej4datos.txt", "w");
	    fwrite($archivo, $frase);
	    fclose($archivo);
	    
	}
	?>
	<form method="post" action="ej4datosalumno.php">
	    <input type='button' value='Volver' onclick='history.back()'/>
	    <input type="submit" value="mostrar"/>
	</form>
    </body>
</html>
